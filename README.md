# README # 

Este README documenta as etapas necessárias para colocar o seu aplicativo em funcionamento.

## 🚗 Stand Marmelos ##

* Este projeto foi realizado para a cadeira de Ambientes de Desenvolvimento Colaborativo do Curso de Sisteamas e Tecnologias da Informação.

## 📓 Projeto ##

* Este projeto/código foi concebido para criar uma interface entre o utilizador e gestão de uma loja/empresa. Uma empresa pode ser gerida usando este programa, com as funcionalidades implementadas, é possivel criar urilizadores, adicionar qualquer tipo de stock, realizar compras e vendas, etc...
* Versão 1.0.0

## 📥 Pré-Requisitos ##

* Para utilizar este programa deverá ter instalado o Python 3.9.0 no seu computador e também um interpretador de Python à sua escolha, é altamente recomendado que use o Pycharm pois foi lá que o projeto foi desenvolvido, mas fica a seu critério... 

## ⚙️ Instalação ##

* Primeiro deverá de descarregar o nosso projeto/código;
* Em segundo deverá de abrir o projeto no seu interpretador de Python escolhido;
* Para começar a usar o projeto apenas deverá de dar "RUN" ao main.py e já estará a controlar todo o programa;
* ⚠️ATENÇÃO:⚠️ Quando der "RUN" ao main.py devera de clicar no "C" para carregar todos os dados presentes na lista do projeto, se não o fizer, todos os dados que estavam contidos dentro do programa não vão aparecer.

## 🔩 Desenvolvimento ##

* Este projeto/código é bastante útil para qualquer tipo de loja que tenha um stock de produtos, todo o código pode ser adaptado, portanto dá para qualquer tipo de negócio.
* Este programa facilita muito a gestão do stock presente nas lojas/organizações, assim como também na gestão dos utilizadores e também no resgisto de compras realizadas.

## 🔨 Construido com: ##

* Pycharm Community Edition, link: https://www.jetbrains.com/pycharm/
* Trello, link: https://.trello.com/
* Bitbucket, link: https://.bitbucket.org/

## 📝 Autores ##

* Duarte Camões;
* Filipe Almeida;
* Gonçalo Guerreiro;
* Tiago Pereira;

* Caso tenha qualquer tipo de dúvida, pode contactar qualquer representante da equipa.
* Caso encontre algum erro no projeto, agradecemos que nos contacte.

## 📧 Contactos ##

* Duarte Camões, email: a70627@ualg.pt
* Tiago Pereira, email: a70640@ualg.pt
* Filipe Almeida, email: a70638@ualg.pt
* Gonçalo Guerreiro, email: a70636@ualg.pt


