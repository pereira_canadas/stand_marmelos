from io_terminal import imprime_lista

nome_ficheiro_lista_de_veiculos = "lista_de_veiculos.pk"


def cria_novo_veiculo():
    """ Pede ao utilizador para introduzir um novo veiculo

        :return: dicionario com um veiculo na forma
            {"marca": <<marca>>, "modelo": <<modelo>>, "velocidades": <<velocidades>>, "tipo_caixa": <<tipo_caixa>>,
             "cilindrada": <<cilidrada>>, "matricula": <<matricula>>, "cor": <<cor>>, "ano": <<ano>>, "km": <<km>>,
              "preço": <<preço>>}
    """

    marca = input("Marca? ")
    modelo = input("Modelo? ")
    velocidades = input("Velocidades? ")
    tipo_caixa = input("Tipo de Caixa? ")
    cilindrada = input("Cilindrada? ")
    matricula = input("Matricula? ").upper()
    cor = input("Cor? ")
    ano = input("Ano? ")
    km = input("KM? ")
    preço = input("Preço? ")
    return {"Marca:": marca, "Modelo:": modelo, "Velocidades:": velocidades, "Tipo de Caixa:": tipo_caixa,
            "Cilindrada:": cilindrada, "Matricula:": matricula, "Cor:": cor, "Ano:": ano, "KM:": km, "Preço:": preço}


def imprime_lista_de_veiculos(lista_de_veiculos):
    """imprime a lista de veiculos"""

    imprime_lista(cabecalho="Lista de Veiculos", lista=lista_de_veiculos)
